import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Client as Styletron } from "styletron-engine-atomic";
import { Provider as StyletronProvider } from "styletron-react"; // Fix import statement
import Login from "./components/Login";
import SignUp from "./components/SignUp";

import CreateStoryForm from "./components/CreateStoryForm";
import FeedPage from "./components/FeedPage";
import UserProfile from "./components/UserProfile";
import LoggedOutPage from "./components/LoggedOutPage";

import MyFriends from "./components/MyFriends";
import MyFriendProfilePage from "./components/MyFriendProfilePage";
import PostPage from "./components/CreatePostForm";
import SearchResult from "./components/SearchResult";
import StoryPostPage from "./components/StoryPostPage";
import UpdateProfileImageForm from "./components/UpdateProfileImageForm";
import FriendRequestList from "./components/FriendRequestList";
import { StompSessionProvider } from "react-stomp-hooks";
import ChildComponent from "./ChildComponent";
import PublishComponent from "./PublishComponent";
import ProfilePageHeader from "./components/ProfilePageHeader";
const App = () => {
  const engine = new Styletron();

  return (
    <StompSessionProvider url={"http://10.65.1.103:8081/ws-endpoint"}>
      <PublishComponent />
      <ChildComponent />
      <StyletronProvider value={engine}>
        <Router>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="/signUp" element={<SignUp />} />
            <Route path="/feed" element={<FeedPage />} />
            <Route path="/create-post" element={<PostPage />} />
            <Route path="/create-story" element={<CreateStoryForm />} />
            <Route path="/profile" element={<UserProfile />} />
            <Route path="/edit-profile" element={<UpdateProfileImageForm />} />
            <Route path="/logout-page" element={<LoggedOutPage />} />
            <Route path="/my-friends" element={<MyFriends />} />
            <Route path="/friend-requests" element={<FriendRequestList />} />
            <Route
              path="/my-friend-profile-page/:friendUserId"
              element={<MyFriendProfilePage />}
            />
            <Route
              path="/search-results/:searchText"
              element={<SearchResult />}
            />
            {/* <Route path="/friend-stories" element={<StoriesPage />} /> */}
            <Route path="/stories" element={<StoryPostPage />} />
          </Routes>
        </Router>
      </StyletronProvider>
    </StompSessionProvider>
  );
};

export default App;
