import React, { useState, useEffect } from "react";
import { useSubscription } from "react-stomp-hooks";
import { toast } from "react-toastify";
const ChildComponent = () => {
  const [message, setMessage] = useState("");
  const [showAlert, setShowAlert] = useState(false);
  // Subscribing to the topic that we have opened in our spring boot app
  console.log("Goint to execute useSubscription");
  useSubscription("/topic/reply", (receivedMessage) => {
    console.log("Received Message is: ", receivedMessage);
    setMessage(receivedMessage.body);
    setShowAlert(true);
  });
  console.log("Message is: ", message);
  useEffect(() => {
    if (showAlert) {
      toast.success(`${message}`);
      setShowAlert(false); // Reset to false after displaying the alert
    }
  }, [showAlert, message]);
  // return null
  //     const [message, setMessage] = useState("");
  //   // Subscribe to the queue that we have opened in our spring boot app
  //   useSubscription('/user/queue/reply', (message) => {setMessage(message.body)});
  //   return (
  //     <div> The message from websocket broker for myself is {message}</div>
  //   )
};
export default ChildComponent;
