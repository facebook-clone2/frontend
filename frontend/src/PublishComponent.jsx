import { useStompClient } from "react-stomp-hooks";
const PublishComponent = () => {
  const stompClient = useStompClient();
  const publishMessage = () => {
    if (stompClient) {
      stompClient.publish({ destination: "/app/broadcast", body: "Mukul" });
      // stompClient.publish({destination: '/app/user-message', body: 'Hello to myself'})
    }
  };
  publishMessage();
};
export default PublishComponent;
