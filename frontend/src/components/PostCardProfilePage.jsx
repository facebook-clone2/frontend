import React, { useState } from "react";
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  Col,
  Container,
  Row,
} from "reactstrap";
import { toast } from "react-toastify";
import Footer from "./Footer";
const PostCardProfilePage = ({ post, reactions, comments }) => {
  console.log("post = ", post);
  console.log("reaction = ", reactions);
  const [selectedEmoji, setSelectedEmoji] = useState(null);
  const [isCommentDialogOpen, setIsCommentDialogOpen] = useState(false);
  const [commentInput, setCommentInput] = useState("");
  const [isCommentSectionCollapsed, setIsCommentSectionCollapsed] =
    useState(true);

  const toggleCommentSection = () => {
    setIsCommentSectionCollapsed(!isCommentSectionCollapsed);
  };
  const handleEmojiSelection = async (emoji, postId) => {
    const userId = parseInt(localStorage.getItem("userId"));
    try {
      const response = await fetch(
        `http://10.65.1.103:8082/v1.0/post-interaction/react?userId=${userId}&postId=${postId}&reactedOn=${emoji}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.ok) {
        console.log(`Reaction ${emoji} added successfully for post ${post.id}`);
      } else {
        console.error("Failed to add reaction");
      }
    } catch (error) {
      console.error("Error during reaction submission:", error);
    }
  };
  const toggleCommentDialog = () => {
    setIsCommentDialogOpen(!isCommentDialogOpen);
  };
  const handleCommentInputChange = (e) => {
    setCommentInput(e.target.value);
  };
  const handleCommentSubmission = async () => {
    const trimmedCommentInput = commentInput.trim();

    if (!trimmedCommentInput) {
      toast.error("Comment is either empty or contain only whitespace.");
      return;
    }
    try {
      const response = await fetch(
        `http://10.65.1.188:8084/v1.0/comment/addComment/${localStorage.getItem(
          "userId"
        )}/${post.id}/${commentInput}`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            content: commentInput,
            emoji: selectedEmoji,
          }),
        }
      );
      if (response.ok) {
        setCommentInput("");
        setSelectedEmoji(null);
        toggleCommentDialog();
      } else {
        console.error("Failed to add comment");
      }
    } catch (error) {
      console.error("Error during comment submission:", error);
    }
  };
  return (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Col md="6">
          <Card style={{ border: "2px solid black" }}>
            <CardBody>
              <div
                className="content-container"
                style={{
                  maxWidth: "100%",
                  maxHeight: "300px", // Adjust maxHeight as needed
                  overflow: "hidden",
                }}
              >
                {post.contentType === "image" && (
                  <div style={{ width: "100%", height: "100%" }}>
                    <CardImg
                      top
                      src={post.contentUrl}
                      alt="Post"
                      className="post-image"
                      style={{
                        width: "100%",
                        height: "300px",
                        objectFit: "contain",
                      }}
                    />
                  </div>
                )}
                {post.contentType === "video" && (
                  <div
                    style={{
                      width: "100%",
                      height: "300px",
                      objectFit: "contain",
                      // overflow: "hidden",
                    }}
                  >
                    <video
                      controls
                      className="post-video"
                      style={{
                        width: "100%",
                        height: "300px",
                        objectFit: "contain", // Ensure the video fits within the container
                      }}
                    >
                      <source src={post.contentUrl} type="video/mp4" />
                    </video>
                  </div>
                )}
              </div>{" "}
              <CardBody>
                <CardText>{post.text}</CardText>
              </CardBody>
              <CardBody>
                <div
                  className="interaction-buttons"
                  style={{
                    padding: "10px",
                    paddingTop: "15px",
                    paddingBottom: "15px",
                  }}
                >
                  <Button
                    onClick={() => handleEmojiSelection(1, reactions.postId)}
                    color="primary"
                    outline
                    style={{ marginRight: "5px", marginBottom: "3px" }}
                  >
                    👍 ({reactions && reactions.one ? reactions.one : 0} )
                  </Button>
                  <Button
                    onClick={() => handleEmojiSelection(2, reactions.postId)}
                    color="danger"
                    outline
                    style={{ marginRight: "5px", marginBottom: "3px" }}
                  >
                    👎 ({reactions && reactions.two ? reactions.two : 0})
                  </Button>
                  <Button
                    onClick={() => handleEmojiSelection(3, reactions.postId)}
                    color="info"
                    outline
                    style={{ marginRight: "5px", marginBottom: "3px" }}
                  >
                    😂 ({reactions && reactions.three ? reactions.three : 0})
                  </Button>
                  <Button
                    onClick={() => handleEmojiSelection(4, reactions.postId)}
                    color="warning"
                    outline
                    style={{ marginRight: "5px", marginBottom: "3px" }}
                  >
                    😢 ({reactions && reactions.four ? reactions.four : 0})
                  </Button>
                  <Button
                    onClick={() => handleEmojiSelection(5, reactions.postId)}
                    color="danger"
                    outline
                    style={{ marginRight: "5px", marginBottom: "3px" }}
                  >
                    😡 ({reactions && reactions.five ? reactions.five : 0})
                  </Button>
                  <Button onClick={toggleCommentDialog} color="secondary">
                    Comment
                  </Button>
                </div>
                {isCommentDialogOpen && (
                  <div
                    style={{
                      width: "80%", // Set width to 80%
                      margin: "0 10px", // Remove margin for left-align
                      backgroundColor: "#F5F5F5",
                      padding: "15px",
                      borderRadius: "10px",
                    }}
                  >
                    <div
                      style={{
                        display: "flex",
                        alignItems: "center",
                      }}
                    >
                      <textarea
                        rows="1"
                        value={commentInput}
                        onChange={handleCommentInputChange}
                        placeholder="Type your comment..."
                        maxLength={1000} // Set maximum length to 1000 characters
                        style={{
                          flex: 1,
                          border: "1px solid #ccc",
                          borderRadius: "5px",
                          padding: "8px",
                          marginRight: "8px",
                          resize: "none",
                          width: "calc(100% - 16px)", // Adjust width considering padding
                        }}
                      />
                      <Button
                        onClick={handleCommentSubmission}
                        color="primary"
                        outline
                      >
                        Submit
                      </Button>
                    </div>
                    <div
                      style={{
                        marginTop: "5px",
                        color: "#888",
                        fontSize: "12px",
                      }}
                    >
                      {commentInput.length} / 1000 characters
                    </div>
                  </div>
                )}
                {comments?.length > 0 && (
                  <div
                    className="comments-section"
                    style={{
                      marginTop: "20px",
                      borderTop: "1px solid #ccc",
                      paddingTop: "20px",
                    }}
                  >
                    <p
                      style={{
                        fontSize: "16px",
                        marginBottom: "15px",
                      }}
                    >
                      <span
                        onClick={toggleCommentSection}
                        style={{ cursor: "pointer" }}
                      >
                        Comments{" "}
                        {isCommentSectionCollapsed ? (
                          <span style={{ color: "grey" }}>&#x25BC;</span>
                        ) : (
                          <span style={{ color: "grey" }}>&#x25B2;</span>
                        )}
                      </span>
                    </p>
                    {!isCommentSectionCollapsed && (
                      <>
                        {comments.map((comment, index) => (
                          <p
                            key={index}
                            className="comment"
                            style={{
                              marginBottom: "15px",
                              padding: "10px",
                              borderRadius: "8px",
                              backgroundColor: "#f8f8f8",
                              boxShadow: "0 2px 4px rgba(0, 0, 0, 0.1)",
                              fontSize: "16px",
                            }}
                          >
                            <strong
                              style={{ fontWeight: "bold", marginRight: "5px" }}
                            >
                              {comment.name != null
                                ? comment.name
                                : "Deleted User"}{" "}
                            </strong>
                            :{" "}
                            <span style={{ wordWrap: "break-word" }}>
                              {comment.content}
                            </span>
                          </p>
                        ))}
                      </>
                    )}
                  </div>
                )}
              </CardBody>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Footer />
    </Container>
  );
};
export default PostCardProfilePage;
