import React from "react";
import { Card, CardImg, CardBody, CardTitle, Button } from "reactstrap";
import { useNavigate } from "react-router-dom";

const FriendCard = ({ id, imageUrl, name }) => {
  const navigate = useNavigate();

  const handleViewProfileOnClick = () => {
    navigate(`/my-friend-profile-page/${id}`);
  };
  return (
    <Card
      style={{
        border: "2px solid black",
        height: "380px",
        width: "300px",
        // display: "flex",
        // flexDirection: "column",
        marginRight: "20px",
        marginBottom: "20px",
      }}
    >
      <CardBody className="text-center">
        <CardTitle tag="h5">{name}</CardTitle>
      </CardBody>
      <CardBody>
        <img
          alt="Card cap"
          src={imageUrl}
          style={{ width: "100%", height: "200px", objectFit: "contain" }}
        />
      </CardBody>
      <CardBody className="d-flex justify-content-center align-items-center">
        <Button
          color="warning"
          onClick={handleViewProfileOnClick}
          style={{ marginLeft: "37px" }}
        >
          View Profile
        </Button>
      </CardBody>
    </Card>
  );
};

export default FriendCard;
