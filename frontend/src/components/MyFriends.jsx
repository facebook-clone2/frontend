import React, { useState, useEffect } from "react";
import FriendCard from "./FriendCard";
import PeopleCard from "./PeopleCard";
import "bootstrap/dist/css/bootstrap.min.css";
import ProfilePageHeader from "./ProfilePageHeader";
import Footer from "./Footer";

const MyFriends = () => {
  const [friends, setFriends] = useState([]);
  const [people, setPeople] = useState([]);

  useEffect(() => {
    const id = localStorage.getItem("userId");
    if (id) {
      fetch(`http://10.65.1.112:8080/friends/getfriends/${id}`)
        .then((response) => response.json())
        .then((data) => setFriends(data))
        .catch((error) => console.error("Error fetching friends:", error));

      fetch("http://10.65.1.188:8081/v1.0/users/getAllUsers")
        .then((response) => response.json())
        .then((data) => setPeople(data))
        .catch((error) => console.error("Error fetching all users:", error));
    }
  }, []);

  return (
    <div>
      <ProfilePageHeader></ProfilePageHeader>

      <div className="container mt-5" style={{ padding: "75px 0" }}>
        <h2>My Friends</h2>
        <div className="row">
          {friends &&
            friends.map((friend) => (
              <div key={friend.id} className="col-md-4">
                <FriendCard
                  id={friend.id}
                  imageUrl={friend.imageUrl}
                  name={friend.name}
                />
              </div>
            ))}
        </div>
        <hr />
        <h2>All People</h2>
        <div className="row">
          {people &&
            people.map((person) => (
              <div key={person.id} className="col-md-4">
                <PeopleCard
                  id={person.id}
                  imageUrl={person.imageUrl}
                  name={person.name}
                />
              </div>
            ))}
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default MyFriends;
