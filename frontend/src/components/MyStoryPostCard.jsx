import {
  Card,
  CardBody,
  CardImg,
  CardText,
  Col,
  Container,
  Row,
} from "reactstrap";
import Footer from "./Footer";

const MyStoryPostCard = ({ post }) => {
  console.log("Stories is: ", post);
  return (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Col md="6">
          <Card style={{ border: "2px solid black" }}>
            <CardBody>
              {post && post.contentType === "image" && (
                <div style={{ maxHeight: "300px", overflow: "hidden" }}>
                  <CardImg
                    top
                    src={post.contentUrl}
                    alt="Post"
                    className="post-image"
                    style={{
                      objectFit: "contain",
                      width: "100%",
                      height: "300px",
                    }}
                  />
                </div>
              )}
              {post && post.contentType === "video" && (
                <div style={{ maxHeight: "300px", overflow: "hidden" }}>
                  <video
                    controls
                    className="post-video"
                    style={{
                      objectFit: "contain",
                      width: "100%",
                      height: "300px",
                    }}
                  >
                    <source src={post.contentUrl} type="video/mp4" />
                  </video>
                </div>
              )}
              {/* {post?.contentType !== "image" &&
                post?.contentType !== "video" && (
                  <CardBody>
                    <CardText>{post?.text}</CardText>
                  </CardBody>
                )} */}
              <CardBody>
                <CardText>{post?.text}</CardText>
              </CardBody>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Footer />
    </Container>
  );
};

export default MyStoryPostCard;
