import React, { useEffect, useState } from "react";
import PostCardProfilePage from "./PostCardProfilePage";
import { useParams } from "react-router-dom";
import FriendPageHeader from "./FriendPageHeader";
import Footer from "./Footer";

const MyFriendProfilePage = () => {
  const [userInfo, setUserInfo] = useState({});
  const [userPosts, setUserPosts] = useState([]);
  const { friendUserId } = useParams();
  const [reactions, setReactions] = useState([]);
  const [comments, setComments] = useState([]);
  const token = localStorage.getItem("token");
  console.log("User data", userInfo);

  useEffect(() => {
    fetch(
      `http://10.65.1.188:8081/v1.0/users/getDetailsOfPerson/${friendUserId}/${localStorage.getItem(
        "userId"
      )}`
    )
      .then((response) => response.json())
      .then((data) => setUserInfo(data))
      .catch((error) => console.error("Error fetching user info:", error));
  }, [friendUserId, token]);

  useEffect(() => {
    if (
      userInfo.publicProfile === 1 ||
      (userInfo.publicProfile === 0 && userInfo.areFriends)
    ) {
      fetch(`http://10.65.1.103:8081/v1.0/post/get-post?userId=${friendUserId}`)
        .then((response) => response.json())
        .then((data) => setUserPosts(data))
        .catch((error) => console.error("Error fetching user posts:", error));
    }
  }, [friendUserId, userInfo.publicProfile, userInfo.areFriends]);

  useEffect(() => {
    if (
      userInfo.publicProfile === 1 ||
      (userInfo.publicProfile === 0 && userInfo.areFriends)
    ) {
      const postIds = userPosts.map((post) => post.id);
      fetch(
        "http://10.65.1.103:8082/v1.0/post-interaction/getReactionOnPosts",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            postIds: postIds,
          }),
        }
      )
        .then((response) => response.json())
        .then((data) => setReactions(data))
        .catch((error) => console.error("Error fetching reactions:", error));
    }
  }, [userPosts, userInfo.publicProfile, userInfo.areFriends]);

  useEffect(() => {
    console.log("Inside use effect to get comments;");
    if (
      userInfo.publicProfile === 1 ||
      (userInfo.publicProfile === 0 && userInfo.areFriends)
    ) {
      const fetchCommentsForPost = async (postId) => {
        try {
          const commentsResponse = await fetch(
            `http://10.65.1.188:8084/v1.0/comment/getCommentByPostId/${postId}`
          );
          const commentsData = await commentsResponse.json();
          console.log("Got comments on post:", commentsData);
          setComments((prevComments) => [
            ...prevComments,
            { postId: postId, comments: commentsData },
          ]);
        } catch (error) {
          console.error("Error fetching comments:", error);
        }
      };

      userPosts.forEach((post) => {
        fetchCommentsForPost(post.id);
      });
    }
  }, [userPosts, userInfo.publicProfile, userInfo.areFriends]);

  return (
    <div>
      <FriendPageHeader
        id={userInfo.id}
        image={userInfo.imageUrl}
        name={userInfo.name}
      ></FriendPageHeader>
      <div style={{ padding: "95px 0" }}>
        <strong
          style={{
            display: "block",
            textAlign: "center",
            fontSize: "20px",
          }}
        >
          {userInfo?.name}'s Posts
        </strong>
        {(userInfo.publicProfile === 1 ||
          (userInfo.publicProfile === 0 && userInfo.areFriends)) && (
          <div className="post-container">
            {userPosts.map((post, index) => (
              <PostCardProfilePage
                key={post.id}
                post={post}
                reactions={reactions[index]}
                comments={comments.find((c) => c.postId === post.id)?.comments}
              />
            ))}
          </div>
        )}
        {userInfo.publicProfile === 0 && !userInfo.areFriends && (
          <div>Follow to view profile</div>
        )}
      </div>
      <Footer />
    </div>
  );
};

export default MyFriendProfilePage;
