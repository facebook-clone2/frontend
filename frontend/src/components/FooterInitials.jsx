import React from "react";

const FooterInitials = () => {
  const footerStyle = {
    position: "fixed",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#343a40",
    padding: "8px",
    boxShadow: "0px -2px 10px rgba(0, 0, 0, 0.1)",
  };

  return (
    <div className="footer" style={footerStyle}>
      <p
        style={{
          color: "white",
          fontWeight: "bold",
          display: "flex",
          justifyContent: "center",
        }}
      >
        © 2024 Lets Connect All Rights Reserved
      </p>
    </div>
  );
};

export default FooterInitials;
