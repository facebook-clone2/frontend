import React from "react";
import { Button, Row, Col } from "reactstrap";
import { Link } from "react-router-dom";

const Footer = () => {
  const footerStyle = {
    position: "fixed",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: "#343a40",
    padding: "8px",
    boxShadow: "0px -2px 10px rgba(0, 0, 0, 0.1)",
  };

  return (
    <div className="footer" style={footerStyle}>
      <Row className="justify-content-center">
        <Col xs="auto">
          <Button tag={Link} to="/stories" color="link" style={buttonStyle}>
            <strong>Friend Stories</strong>
          </Button>
        </Col>
        <Col xs="auto">
          <Button
            tag={Link}
            to="/friend-requests"
            color="link"
            style={buttonStyle}
          >
            <strong>Friend Requests</strong>
          </Button>
        </Col>
        <Col xs="auto">
          <Button tag={Link} to="/feed" color="link" style={buttonStyle}>
            <strong>My Feed</strong>
          </Button>
        </Col>
        <Col xs="auto">
          <Button tag={Link} to="/my-friends" color="link" style={buttonStyle}>
            <strong>My Friends</strong>
          </Button>
        </Col>
        <Col xs="auto">
          <Button tag={Link} to="/create-post" style={createPostButtonStyle}>
            <strong>Create Post</strong>
          </Button>
        </Col>

        <Col xs="auto">
          <Button tag={Link} to="/create-story" style={createStoryButtonStyle}>
            <strong>Create Story</strong>
          </Button>
        </Col>
      </Row>
    </div>
  );
};

const buttonStyle = {
  border: "1px solid #fff",
  backgroundColor: "#343a40",
  color: "#fff",
  textDecoration: "none",
  textTransform: "capitalize",
  margin: "4px",
  borderRadius: "20px",
  transition: "background-color 0.3s ease",
};

const createPostButtonStyle = {
  backgroundColor: "#855E06",
  color: "#fff",
  textDecoration: "none",
  textTransform: "capitalize",
  margin: "4px",
  borderRadius: "20px",
  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.2)",
  transition: "background-color 0.3s ease, box-shadow 0.3s ease",
};

const createStoryButtonStyle = {
  backgroundColor: "#2AB4D6",
  color: "#fff",
  textDecoration: "none",
  textTransform: "capitalize",
  margin: "4px",
  borderRadius: "20px",
  boxShadow: "0px 4px 10px rgba(0, 0, 0, 0.2)",
  transition: "background-color 0.3s ease, box-shadow 0.3s ease",
};

export default Footer;
