import React, { useState, useEffect } from "react";
import { Table, Button } from "reactstrap";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import ProfilePageHeader from "./ProfilePageHeader";
import Footer from "./Footer";
const FriendRequestList = () => {
  const navigate = useNavigate();
  const [friendRequests, setFriendRequests] = useState([]);
  useEffect(() => {
    const fetchFriendRequests = async () => {
      try {
        const response = await fetch(
          `http://10.65.1.112:8080/friends/getFriendRequests/${localStorage.getItem(
            "userId"
          )}`
        );
        const data = await response.json();
        setFriendRequests(data);
      } catch (error) {
        console.error("Error fetching friend requests:", error);
      }
    };
    fetchFriendRequests();
  }, []);
  const handleAction = async (userId, action) => {
    try {
      const response = await fetch(
        `http://10.65.1.112:8080/friends/${action}FriendRequest/${userId}/${localStorage.getItem(
          "userId"
        )}`,
        {
          method: "POST",
        }
      );
      const data = await response.text();
      switch (data) {
        case "Friend request approved successfully!":
          toast.success(data);
          setTimeout(() => {
            navigate("/my-friends");
          }, 3000); // 3000 milliseconds = 3 seconds
          break;
        case "Friend request approval failed!":
          toast.error(data);
          setTimeout(() => {
            navigate("/my-friends");
          }, 3000);
          break;
        case "Friend request declined successfully!":
          toast.success(data);
          setTimeout(() => {
            navigate("/friend-requests");
          }, 3000);
          break;
        case "Friend request decline failed!":
          toast.error(data);
          setTimeout(() => {
            navigate("/friend-requests");
          }, 3000);
          break;
        default:
          toast.warning("Unknown response");
      }
    } catch (error) {
      console.error("Error handling action:", error);
      toast.error("Error handling action");
    }
  };
  return (
    <>
      <ProfilePageHeader></ProfilePageHeader>
      <div style={{ padding: "75px 0" }}>
        <h2 className="text-center mb-4">FRIEND REQUESTS</h2>
        <Table>
          <thead>
            <tr>
              <th>Image</th>
              <th>Name</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {friendRequests.map((request) => (
              <tr key={request.senderUserId}>
                <td>
                  <img
                    src={request.imageUrl}
                    alt={request.name}
                    style={{
                      width: "50px",
                      height: "50px",
                      borderRadius: "50%",
                    }}
                  />
                </td>
                <td>{request.name}</td>
                <td>
                  <Button
                    color="success"
                    onClick={() =>
                      handleAction(request.senderUserId, "approve")
                    }
                  >
                    Approve
                  </Button>{" "}
                  <Button
                    color="danger"
                    onClick={() =>
                      handleAction(request.senderUserId, "decline")
                    }
                  >
                    Decline
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
      <Footer></Footer>
    </>
  );
};
export default FriendRequestList;
