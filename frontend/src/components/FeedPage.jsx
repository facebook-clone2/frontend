import React, { useState, useEffect } from "react";
import ProfilePageHeader from "./ProfilePageHeader";
import Footer from "./Footer";
import PostCard from "./PostCard";

const FeedPage = () => {
  const [posts, setPosts] = useState([]);
  const [reactions, setReactions] = useState([]);
  const [comments, setComments] = useState([]);
  console.log("Post: ", posts);

  useEffect(() => {
    const fetchPostsAndReactions = async () => {
      try {
        const postsResponse = await fetch(
          `http://10.65.1.112:8080/friends/getPostFromFriends/${localStorage.getItem(
            "userId"
          )}`
        );
        const postsData = await postsResponse.json();
        setPosts(postsData);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };

    fetchPostsAndReactions();
  }, []);

  useEffect(() => {
    if (posts.length > 0) {
      const postIds = posts.map((post) => post.id);
      fetch(
        "http://10.65.1.103:8082/v1.0/post-interaction/getReactionOnPosts",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            postIds: postIds,
          }),
        }
      )
        .then((response) => response.json())
        .then((data) => setReactions(data))
        .catch((error) => console.error("Error fetching reactions:", error));
    }
  }, [posts]);

  useEffect(() => {
    const fetchCommentsForPost = async (postId) => {
      try {
        const commentsResponse = await fetch(
          `http://10.65.1.188:8084/v1.0/comment/getCommentByPostId/${postId}`
        );
        const commentsData = await commentsResponse.json();
        setComments((prevComments) => [
          ...prevComments,
          { postId: postId, comments: commentsData },
        ]);
      } catch (error) {
        console.error("Error fetching comments:", error);
      }
    };

    posts.forEach((post) => {
      fetchCommentsForPost(post.id);
    });
  }, [posts]);

  return (
    <div className="feed-page">
      <ProfilePageHeader />
      <div className="content" style={{ padding: "75px 0" }}>
        <div className="left-sidebar">
          {posts?.map((post, index) => (
            <PostCard
              key={post.id}
              post={post}
              reactions={reactions[index]}
              comments={comments.find((c) => c.postId === post.id)?.comments}
            />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default FeedPage;
