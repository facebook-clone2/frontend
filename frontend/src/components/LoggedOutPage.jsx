import React from "react";
import { Button } from "reactstrap"; // Assuming you have Reactstrap
import { useNavigate } from "react-router-dom";
import HeaderSiteName from "./HeaderSiteName";

const LoggedOutPage = () => {
  const navigate = useNavigate();
  const onLoginClick = () => {
    navigate("/");
  };

  return (
    <div className="text-center mt-5">
      <HeaderSiteName />
      <div style={{ padding: "75px 0" }}>
        <h1>You Have Logged Out Successfully. Bye!!</h1>
        <br></br>
        <Button color="info" onClick={onLoginClick}>
          <strong>Login</strong>
        </Button>
      </div>
    </div>
  );
};

export default LoggedOutPage;
