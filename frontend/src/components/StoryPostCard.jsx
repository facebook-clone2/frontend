import {
  Card,
  CardBody,
  CardImg,
  CardText,
  Col,
  Container,
  Row,
} from "reactstrap";
import Footer from "./Footer";

const StoryPostCard = ({ post }) => {
  console.log("Post is: ", post);
  return (
    <Container className="mt-5">
      <Row className="justify-content-center">
        <Col md="6">
          <Card style={{ border: "2px solid black" }}>
            <CardBody>
              <div className="user-name" style={{ marginBottom: "10px" }}>
                Posted by : <strong>{post.name}</strong>
              </div>

              {post && post.contentType === "image" && (
                <div style={{ maxHeight: "300px", overflow: "hidden" }}>
                  <CardImg
                    top
                    src={post.contentUrl}
                    alt="Post"
                    className="post-image"
                    style={{
                      objectFit: "contain",
                      height: "300px",
                      width: "100%",
                    }}
                  />
                </div>
              )}
              {post.contentType === "video" && (
                <div
                  style={{
                    maxHeight: "300px",
                    overflow: "hidden",
                    width: "100%",
                  }}
                >
                  <video
                    controls
                    className="post-video"
                    style={{
                      objectFit: "contain",
                      width: "100%",
                      height: "300px",
                    }}
                  >
                    <source src={post.contentUrl} type="video/mp4" />
                  </video>
                </div>
              )}
              {/* {post?.contentType !== "image" &&
                post?.contentType !== "video" && (
                  <CardBody>
                    <CardText>{post?.text}</CardText>
                  </CardBody>
                )} */}
              <CardBody>
                <CardText>{post?.text}</CardText>
              </CardBody>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Footer />
    </Container>
  );
};

export default StoryPostCard;
