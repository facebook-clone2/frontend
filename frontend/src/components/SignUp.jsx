import React, { useState, useEffect } from "react";
import { useNavigate, Link as RouterLink } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { Button, Input, Col, Row, Form, FormGroup, Label } from "reactstrap";
import { toast } from "react-toastify";
import AWS from "aws-sdk";
import HeaderSiteName from "./HeaderSiteName";
import FooterInitials from "./FooterInitials";
const s3 = new AWS.S3({
  region: "ap-south-1",
  accessKeyId: "AKIA3LCNRWEZ5CTRMQ4V",
  secretAccessKey: "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",
});
const SignUp = () => {
  const navigate = useNavigate();
  const { handleSubmit, control, formState } = useForm();
  const { errors } = formState;
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [selectedImage, setSelectedImage] = useState(null);
  const [submitDisabled, setSubmitDisabled] = useState(true);
  const [isImageValid, setIsImageValid] = useState(false);
  const [imageUrl, setImageUrl] = useState("");
  useEffect(() => {
    setSubmitDisabled(!imageUrl); // Disable submit button until imageUrl is set
  }, [imageUrl]);
  const validPassword = (password) => {
    return /^(?=.*[A-Za-z0-9])(?=.*[!@#$%^&*()_+])[A-Za-z0-9!@#$%^&*()_+]{8,}$/.test(
      password
    );
  };
  const handleKeyPress = (e) => {
    if (e.key === "Enter" && e.target.tagName !== "INPUT") {
      e.preventDefault(); // Prevent form submission if the Enter key is pressed inside an input field
      handleSignUp();
    }
  };
  const isEmailValid = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  };
  const handleSignUp = async (data) => {
    try {
      if (!isEmailValid(data.email)) {
        alert("Please enter a valid email address.");
        return;
      }
      if (!validPassword(data.password)) {
        alert(
          "Password should be at least 8 characters long and contain at least one special character."
        );
        return;
      }
      let imageUrl = null;
      if (selectedImage) {
        const file = selectedImage;
        const key = `user-profiles/${Date.now()}-${file.name}`;
        const imageData = await s3
          .upload({
            Bucket: "engatiinstagram",
            Key: key,
            Body: file,
            ACL: "public-read",
          })
          .promise();
        imageUrl = imageData.Location;
        setImageUrl(imageUrl);
        console.log("Image uploaded successfully:", imageUrl);
      }
      const response = await fetch(
        "http://10.65.1.188:8081/v1.0/users/signup",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            email: data.email,
            name: data.name,
            password: data.password,
            imageUrl: imageUrl,
          }),
        }
      );
      if (response.ok) {
        navigate("/");
        toast.success("Registered Successfully");
      } else {
        toast.error("Error occurred! User already Exist");
        navigate("/");
      }
    } catch (error) {
      console.error("Error during SignUp:", error);
    }
  };
  const handleImageChange = async (e) => {
    const file = e.target.files[0];
    if (!file) {
      console.error("No file selected");
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      const buffer = reader.result;
      const uint = new Uint8Array(buffer);
      let bytes = [];
      uint.forEach((byte) => {
        bytes.push(byte.toString(16));
      });
      const hex = bytes.join("").toUpperCase();
      const jpegMagicNumber = "FFD8FF";
      const pngMagicNumber = "89504E47";
      const jpgMagicNumber = "FFD8";
      const validExtensions = ["jpg", "jpeg", "png"];
      // Check both extension and magic number
      if (
        (hex.startsWith(jpegMagicNumber) ||
          hex.startsWith(pngMagicNumber) ||
          hex.startsWith(jpgMagicNumber)) &&
        validExtensions.includes(file.name.split(".").pop().toLowerCase())
      ) {
        setSelectedImage(file);
        setIsImageValid(true); // Set state to true if both conditions are met
      } else {
        setSelectedImage(null); // Reset selected image if it doesn't meet the criteria
        setIsImageValid(false); // Set state to false
        // File is not a valid image, notify user
        alert(
          "Selected file is not a valid image or doesn't have a valid extension (jpg, jpeg, or png)."
        );
      }
    };
    reader.readAsArrayBuffer(file);
  };
  return (
    <div>
      <HeaderSiteName />
      <Row className="justify-content-center align-items-center min-vh-100">
        <Col md={4}>
          <Form
            onSubmit={handleSubmit(handleSignUp)}
            onKeyPress={handleKeyPress}
            className="form-container p-4 shadow rounded"
          >
            <h2 className="text-center mb-4">Sign Up</h2>
            <FormGroup>
              <Label for="email" style={{ fontWeight: "bold" }}>
                Email:
              </Label>
              <Controller
                name="email"
                control={control}
                defaultValue=""
                rules={{
                  required: "Email is required",
                  pattern: {
                    value: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
                    message: "Invalid email address",
                  },
                }}
                render={({ field }) => (
                  <Input
                    {...field}
                    className="rounded-pill py-2 px-3 mb-3"
                    type="email"
                    invalid={!!errors.email}
                  />
                )}
              />
            </FormGroup>
            <FormGroup>
              <Label for="name" style={{ fontWeight: "bold" }}>
                Name:
              </Label>
              <Controller
                name="name"
                control={control}
                defaultValue=""
                rules={{ required: "Name is required" }}
                render={({ field }) => (
                  <Input
                    {...field}
                    className="rounded-pill py-2 px-3 mb-3"
                    type="text"
                    invalid={!!errors.name}
                  />
                )}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password" style={{ fontWeight: "bold" }}>
                Password:
              </Label>
              <Controller
                name="password"
                control={control}
                defaultValue=""
                rules={{
                  required: "Password is required",
                  pattern: {
                    value:
                      /^(?=.*[A-Za-z0-9])(?=.*[!@#$%^&*()_+])[A-Za-z0-9!@#$%^&*()_+]{8,}$/,
                    message:
                      "Password should be at least 8 characters long and contain at least one special character.",
                  },
                }}
                render={({ field }) => (
                  <Input
                    {...field}
                    className="rounded-pill py-2 px-3 mb-3"
                    type="password"
                    onBlur={(e) =>
                      setIsPasswordValid(validPassword(e.target.value))
                    }
                    invalid={!!errors.password}
                  />
                )}
              />
            </FormGroup>
            <FormGroup>
              <Label for="imageInput" style={{ fontWeight: "bold" }}>
                Select Profile Image:
              </Label>
              <Input
                type="file"
                id="imageInput"
                className="form-control"
                onChange={handleImageChange}
              />
            </FormGroup>
            <div className="button-container text-center mt-3">
              <Button
                type="submit"
                color="secondary"
                disabled={!isImageValid}
                style={{ backgroundColor: "blue" }}
                className="rounded-pill px-4"
              >
                <strong>Sign Up</strong>
              </Button>
            </div>
            {formState.errors.email && (
              <p className="error-message mt-3">
                {formState.errors.email.message}
              </p>
            )}
            {formState.errors.name && (
              <p className="error-message">{formState.errors.name.message}</p>
            )}
            {formState.errors.password && (
              <p className="error-message">
                {formState.errors.password.message}
              </p>
            )}
            <div className="link-container mt-3 text-center">
              <RouterLink to="/" className="login-link">
                <Button color="warning" className="rounded-pill px-4">
                  <strong>Already have an account? Login</strong>
                </Button>
              </RouterLink>
            </div>
          </Form>
        </Col>
      </Row>
      <FooterInitials></FooterInitials>
    </div>
  );
};
export default SignUp;
