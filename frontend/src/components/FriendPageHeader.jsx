import React from "react";
import { Navbar, NavbarBrand, Nav, NavItem, NavLink, Button } from "reactstrap";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";

const FriendPageHeader = ({ id, image, name }) => {
  const navigate = useNavigate();
  const userId = localStorage.getItem("userId");

  const handleButtonClick = () => {
    fetch(`http://10.65.1.112:8080/friends/addfriend/${userId}/${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        console.log("Response is: ", response);
        if (!response.ok) {
          throw new Error(`Failed to add friend with id ${id}`);
        }
        return response.text(); // Assume plain text response
      })
      .then((message) => {
        console.log("Message is: ", message);
        handleResponse(message);
      })
      .catch((error) => {
        toast.error("Error adding friend");
        console.error("Fetch error:", error);
      });
  };
  const handleResponse = (message) => {
    switch (message) {
      case "Friends Already!":
        toast.info("Friends Already!");
        break;
      case "Friends added successfully!":
        toast.success("Successfully added friend");
        navigate("/my-friends");
        break;
      case "Friend request sent successfully!":
        toast.success("Friend request sent successfully!");
        break;
      case "Friend request already sent":
        toast.info("Friend request already sent");
        break;
      case "Invalid Request!":
        toast.error("Invalid request!");
        break;
      default:
        toast.error("Unexpected response from the server");
        break;
    }
  };

  return (
    <Navbar
      color="dark"
      light
      expand="md"
      className="navbar-custom"
      fixed="top"
    >
      {/* Left side of the navbar */}
      <NavbarBrand>
        <div className="d-flex align-items-center">
          <img
            src={image} // Replace with the actual path or URL of the profile picture
            alt="Profile"
            className="rounded-circle mr-2"
            height="60" // Increase the height of the image
          />
        </div>
      </NavbarBrand>

      {/* Center of the navbar (User's name) */}
      <Nav className="mr-auto" navbar>
        <NavItem>
          <span className="navbar-text" style={{ color: "white" }}>
            <strong>{name}'s Profile</strong>
          </span>
        </NavItem>
      </Nav>

      {/* Right side of the navbar */}

      <Nav navbar className="ml-auto">
        <NavItem>
          <Button
            color="link"
            onClick={handleButtonClick}
            style={{ color: "white" }}
          >
            Add Friend
          </Button>
        </NavItem>
      </Nav>
      <Nav navbar className="ml-auto">
        <NavItem>
          <NavLink
            href="/profile"
            className="nav-link"
            style={{ color: "white" }}
          >
            Back To My Profile
          </NavLink>
        </NavItem>
      </Nav>
    </Navbar>
  );
};

export default FriendPageHeader;
