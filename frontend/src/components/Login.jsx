import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, Col, Form, FormGroup, Input, Label, Row } from "reactstrap";
import { toast } from "react-toastify";
import { Link as RouterLink } from "react-router-dom";
import HeaderSiteName from "./HeaderSiteName";
import FooterInitials from "./FooterInitials";

const Login = () => {
  const navigate = useNavigate();
  const [userLogin, setUserLogin] = useState({ email: "", password: "" });
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleUserLoginChange = (e) => {
    setUserLogin({ ...userLogin, [e.target.name]: e.target.value });
  };

  const handleLogin = async () => {
    try {
      const response = await fetch("http://10.65.1.188:8081/v1.0/users/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(userLogin),
      });

      if (response.ok) {
        const result = await response.json();
        const { token, id } = result;

        localStorage.setItem("token", token);
        localStorage.setItem("userId", id);
        setIsLoggedIn(true);
        const a = localStorage.getItem("userId");
        console.log(a);
        navigate("/feed");
        toast.success("Successfully logged in");
      } else {
        toast.error("Invalid Credentials");
      }
    } catch (error) {
      console.error("Error during login:", error);
    }
  };

  const handleFormSubmit = (e) => {
    e.preventDefault();
    handleLogin();
  };

  const handleKeyPress = (e) => {
    if (e.key === "Enter") {
      handleLogin();
    }
  };

  return (
    <div>
      <HeaderSiteName />
      {isLoggedIn ? (
        <p>You are already logged in.</p>
      ) : (
        <Row className="justify-content-center align-items-center min-vh-100">
          <Col md={4}>
            <Form
              className="form-container p-4 shadow rounded"
              onSubmit={handleFormSubmit}
            >
              <h2 className="text-center mb-4">User Login</h2>
              <FormGroup>
                <Label for="email" className="mb-2 text-muted">
                  <strong>Email:</strong>
                </Label>
                <Input
                  type="email"
                  name="email"
                  id="email"
                  value={userLogin.email}
                  onChange={handleUserLoginChange}
                  required
                  className="rounded-pill py-2 px-3"
                />
              </FormGroup>
              <FormGroup>
                <Label for="password" className="mb-2 text-muted">
                  <strong>Password:</strong>
                </Label>
                <Input
                  type="password"
                  name="password"
                  id="password"
                  value={userLogin.password}
                  onChange={handleUserLoginChange}
                  required
                  className="rounded-pill py-2 px-3"
                />
              </FormGroup>
              <div className="button-container text-center mt-4">
                <Button
                  type="submit"
                  color="secondary"
                  className="rounded-pill px-4"
                  onKeyPress={handleKeyPress}
                >
                  <strong>Login</strong>
                </Button>
              </div>
              <div className="link-container mt-3 text-center">
                <RouterLink to="/signup" className="login-link">
                  <Button color="warning" className="rounded-pill px-4">
                    <strong>Want to create an account? SignUp</strong>
                  </Button>
                </RouterLink>
              </div>
            </Form>
          </Col>
        </Row>
      )}
      <FooterInitials></FooterInitials>
    </div>
  );
};

export default Login;
