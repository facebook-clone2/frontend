import React from "react";
import { Card, CardBody, CardTitle, Button } from "reactstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const PeopleCard = ({ id, imageUrl, name }) => {
  const navigate = useNavigate();
  const userId = localStorage.getItem("userId");

  const handleAddFriendOnClick = () => {
    fetch(`http://10.65.1.112:8080/friends/addfriend/${userId}/${id}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((response) => {
        console.log("Response is: ", response);
        if (!response.ok) {
          throw new Error(`Failed to add friend with id ${id}`);
        }
        navigate("/my-friends");
        return response.text();
        // Assume plain text response
      })
      .then((message) => {
        console.log("Message is: ", message);
        handleResponse(message);
      })
      .catch((error) => {
        toast.error("Error adding friend");
        console.error("Fetch error:", error);
      });
  };

  const handleResponse = (message) => {
    switch (message) {
      case "Friends Already!":
        toast.info("Friends Already!");
        break;
      case "Friends added successfully!":
        toast.success("Successfully added friend");
        navigate("/my-friends");
        break;
      case "Friend request sent successfully!":
        toast.success("Friend request sent successfully!");
        break;
      case "Friend request already sent":
        toast.info("Friend request already sent");
        break;
      case "Invalid request!":
        toast.error("Invalid request!");
        break;
      default:
        toast.error("Unexpected response from the server");
        break;
    }
  };

  const handleViewProfileOnClick = () => {
    navigate(`/my-friend-profile-page/${id}`);
  };

  return (
    <Card
      style={{
        border: "2px solid black",
        height: "380px",
        width: "300px",
        // display: "flex",
        // flexDirection: "column",
        marginRight: "20px",
        marginBottom: "20px",
      }}
    >
      <CardBody className="text-center">
        <CardTitle tag="h5">{name}</CardTitle>
      </CardBody>
      <CardBody>
        <img
          alt="Card cap"
          src={imageUrl}
          style={{ width: "100%", height: "200px", objectFit: "contain" }}
        />
      </CardBody>
      <CardBody className="text-center">
        <Button color="primary" onClick={handleAddFriendOnClick}>
          Add Friend
        </Button>
        <Button
          color="warning"
          onClick={handleViewProfileOnClick}
          style={{ marginLeft: "37px" }}
        >
          View Profile
        </Button>
      </CardBody>
    </Card>
  );
};

export default PeopleCard;
