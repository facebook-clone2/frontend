import React, { useState } from "react";
import AWS from "aws-sdk";
import { toast } from "react-toastify";
import {
  Container,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  ButtonGroup,
} from "reactstrap";
import ProfilePageHeader from "./ProfilePageHeader";
import Footer from "./Footer";
const s3 = new AWS.S3({
  region: "ap-south-1",
  accessKeyId: "AKIA3LCNRWEZ5CTRMQ4V",
  secretAccessKey: "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",
});
const CreateStoryForm = () => {
  const [text, setText] = useState("");
  const [contentType, setContentType] = useState("null");
  const [selectedFile, setSelectedFile] = useState(null);
  const [fileName, setFileName] = useState("");
  const [invalidFile, setInvalidFile] = useState(false);
  const userId = localStorage.getItem("userId");
  const [contentUrl, setContentUrl] = useState(null);
  const handleFileChange = async (e) => {
    const file = e.target.files[0];
    if (!file) {
      console.error("No file selected");
      return;
    }
    try {
      const uploadParams = {
        Bucket: "engatiinstagram",
        Key: `user-uploads/${Date.now()}-${file.name}`,
        Body: file,
        ACL: "public-read",
      };
      const result = await s3.upload(uploadParams).promise();
      console.log("File uploaded successfully:", result.Location);
      setContentUrl(result.Location);
      setSelectedFile(true);
    } catch (error) {
      console.error("Error uploading file to S3:", error);
      toast.error("Failed to upload file. Please try again.");
    }
  };
  const handleContentTypeChange = (type) => {
    setContentType(type);
    setSelectedFile(null);
    setFileName("");
    setInvalidFile(false);
    setContentUrl(null);
  };
  const handleSubmitForm = async (e) => {
    e.preventDefault();
    if (!text || (contentType !== "null" && !selectedFile)) {
      toast.error("Please fill in all the required fields");
      return;
    }
    if (contentType !== "null" && !contentUrl) {
      toast.error("Please upload a valid file");
      return;
    }
    const jsonData = {
      userId,
      text,
      contentType,
      contentUrl,
      isStory: true,
    };
    try {
      const response = await fetch(
        "http://10.65.1.103:8081/v1.0/post/create-post",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(jsonData),
        }
      );
      if (response.ok) {
        setText("");
        setContentType("null");
        setSelectedFile(null);
        setFileName("");
        setInvalidFile(false);
        setContentUrl(null);
        console.log("Story added successfully");
        toast.success("Story added successfully");
      } else {
        console.error("Failed to add post");
        toast.error("Failed to add post. Please try again.");
      }
    } catch (error) {
      console.error("Error during post addition:", error);
      toast.error(
        "An error occurred during post addition. Please try again later."
      );
    }
  };
  return (
    <div>
      <ProfilePageHeader />
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ minHeight: "100vh" }}
      >
        <div className="col-md-4 mt-5 p-4 shadow rounded">
          <Container>
            <h1 className="mb-4">Create a Story</h1>
            <Form onSubmit={handleSubmitForm}>
              <FormGroup tag="fieldset">
                <legend>Story Type</legend>
                <ButtonGroup>
                  <Button
                    color={contentType === "image" ? "primary" : "secondary"}
                    onClick={() => handleContentTypeChange("image")}
                  >
                    Image
                  </Button>
                  <Button
                    color={contentType === "video" ? "primary" : "secondary"}
                    onClick={() => handleContentTypeChange("video")}
                  >
                    Video
                  </Button>
                  <Button
                    color={contentType === "null" ? "primary" : "secondary"}
                    onClick={() => handleContentTypeChange("null")}
                  >
                    Text
                  </Button>
                </ButtonGroup>
              </FormGroup>
              <FormGroup>
                <Label for="text">Story text:</Label>
                <Input
                  type="textarea"
                  rows={5}
                  name="text"
                  id="text"
                  value={text}
                  style={{ resize: "none" }}
                  onChange={(e) => setText(e.target.value)}
                />
              </FormGroup>
              {contentType !== "null" && (
                <FormGroup>
                  <Label for="fileInput">
                    Select {contentType === "image" ? "image" : "video"}:
                  </Label>
                  <Input
                    type="file"
                    id="fileInput"
                    accept={contentType === "image" ? "image/*" : "video/*"}
                    onChange={handleFileChange}
                  />
                </FormGroup>
              )}
              <Button
                type="submit"
                color="primary"
                style={{ backgroundColor: "black" }}
                disabled={
                  !(
                    (text && contentType === "null") ||
                    (contentUrl && contentType !== "null")
                  )
                }
              >
                Submit
              </Button>
            </Form>
          </Container>
        </div>
        <Footer />
      </div>
    </div>
  );
};
export default CreateStoryForm;
