import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import {
  Navbar,
  Input,
  NavbarBrand,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Col,
  Row,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Button,
} from "reactstrap";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";

const ProfilePageHeader = () => {
  const navigate = useNavigate();
  const [isPublic, setIsPublic] = useState();
  const [userDetails, setUserDetails] = useState({});
  const [searchText, setSearchText] = useState("");
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [confirmationModalOpen, setConfirmationModalOpen] = useState(false);
  const [confirmationAction, setConfirmationAction] = useState(null);

  const toggleDropdown = () => setDropdownOpen(!dropdownOpen);

  const handleSearch = () => {
    const trimmedSearchText = searchText.trim();
    if (trimmedSearchText === "") {
      toast.error("Please enter a valid search query");
    } else {
      navigate(`/search-results/${trimmedSearchText}`);
    }
  };

  const handleEnterKeyPress = (e) => {
    if (e.key === "Enter") {
      handleSearch();
    }
  };

  const handleLogoutButtonClick = () => {
    setConfirmationAction("logout");
    setConfirmationModalOpen(true);
  };

  const handleDeleteAccountButtonClick = () => {
    setConfirmationAction("deleteAccount");
    setConfirmationModalOpen(true);
  };

  const handleEditImageClick = () => {
    navigate("/edit-profile");
  };

  const handleTogglePrivacyClick = () => {
    const userId = localStorage.getItem("userId");
    if (userId) {
      fetch(
        `http://10.65.1.188:8081/v1.0/users/updateAccountPrivacy/${userId}`,
        {
          method: "GET",
          headers: {
            Authorization: localStorage.getItem("token"),
            "Content-Type": "application/json",
          },
        }
      )
        .then((response) => {
          if (response.ok) {
            setIsPublic((prevState) => !prevState); // Toggle the privacy state
            toast.success(`Profile is now ${isPublic ? "private" : "public"}`);
          } else {
            toast.error("Error toggling privacy");
          }
        })
        .catch((error) => {
          console.error("Error toggling privacy:", error);
          toast.error("Error toggling privacy");
        });
    } else {
      toast.error("User ID not found");
    }
  };

  const handleConfirmation = () => {
    if (confirmationAction === "deleteAccount") {
      const userId = localStorage.getItem("userId");
      if (userId) {
        fetch(`http://10.65.1.188:8081/v1.0/users/deleteUser/${userId}`, {
          method: "DELETE",
          headers: {
            "Content-Type": "application/json",
          },
        })
          .then((response) => {
            if (response.ok) {
              localStorage.removeItem("userId");
              localStorage.removeItem("token");
              toast.error("Account Deleted");
              navigate("/signUp");
            } else {
              toast.error("Error deleting account");
            }
          })
          .catch((error) => {
            console.error("Error deleting account:", error);
            toast.error("Error deleting account");
          });
      } else {
        toast.error("User ID not found");
      }
    } else if (confirmationAction === "logout") {
      localStorage.removeItem("userId");
      localStorage.removeItem("token");
      navigate("/logout-page");
    }
    setConfirmationModalOpen(false);
  };

  useEffect(() => {
    fetch(
      `http://10.65.1.188:8081/v1.0/users/getDetails/${localStorage.getItem(
        "userId"
      )}`
    )
      .then((response) => response.json())
      .then((data) => {
        setUserDetails(data);
        setIsPublic(data.publicProfile === 1);
      })
      .catch((error) => console.error("Error fetching user info:", error));
  }, []);

  return (
    <>
      <Navbar color="dark" dark expand="md" className="navbar-custom fixed-top">
        <Row className="w-100 justify-content-between align-items-center">
          <Col xs="auto">
            <NavbarBrand>
              <Link to="/profile">
                <img
                  src={userDetails.imageUrl}
                  alt="Profile"
                  className="rounded-circle mr-2"
                  height="60"
                />
              </Link>
            </NavbarBrand>
          </Col>

          <Col xs="auto">
            <span
              className="navbar-text"
              style={{ marginLeft: "380px", color: "white" }}
            >
              <strong>{userDetails.name}</strong>
            </span>
          </Col>

          <Col xs="auto">
            <Input
              placeholder="Search for people"
              className="mr-2 search-bar"
              style={{
                width: "170%",
                borderRadius: "20px",
                border: "1px solid #ced4da",
                padding: "8px 15px",
                fontSize: "16px",
                transition: "all 0.3s ease",
              }}
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
              onKeyDown={handleEnterKeyPress}
            />
          </Col>

          <Col xs="auto">
            <Dropdown isOpen={dropdownOpen} toggle={toggleDropdown}>
              <DropdownToggle
                className="dropdown-toggle"
                caret
                style={{
                  backgroundColor: "#343a40",
                  color: "#fff",
                  border: "none",
                  borderRadius: "20px",
                  padding: "8px 15px",
                  fontSize: "16px",
                }}
              >
                Settings
              </DropdownToggle>
              <DropdownMenu right style={{ backgroundColor: "#343a40" }}>
                <style>
                  {`
                    .dropdown-item {
                      color: #fff;
                      font-weight: bold;
                    }
                    .dropdown-item:hover {
                      color: #fff;
                      background-color: #495057; /* Darken the background color on hover */
                    }
                  `}
                </style>
                <DropdownItem onClick={handleEditImageClick}>
                  Edit Profile Image
                </DropdownItem>
                <DropdownItem onClick={handleTogglePrivacyClick}>
                  Toggle {isPublic ? "Private" : "Public"}
                </DropdownItem>
                <DropdownItem onClick={handleDeleteAccountButtonClick}>
                  Delete Account
                </DropdownItem>
                <DropdownItem onClick={handleLogoutButtonClick}>
                  Logout
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </Col>
        </Row>
      </Navbar>

      <Modal
        isOpen={confirmationModalOpen}
        toggle={() => setConfirmationModalOpen(false)}
      >
        <ModalHeader toggle={() => setConfirmationModalOpen(false)}>
          Confirmation
        </ModalHeader>
        <ModalBody>
          Are you sure you want to{" "}
          {confirmationAction === "deleteAccount"
            ? "delete your account?"
            : "logout?"}
        </ModalBody>
        <ModalFooter>
          <Button color="danger" onClick={handleConfirmation}>
            Yes
          </Button>{" "}
          <Button
            color="secondary"
            onClick={() => setConfirmationModalOpen(false)}
          >
            Cancel
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ProfilePageHeader;
