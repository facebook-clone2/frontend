import React, { useState, useEffect } from "react";
import ProfilePageHeader from "./ProfilePageHeader";
import PostCardProfilePage from "./PostCardProfilePage";
import MyStoryPostCard from "./MyStoryPostCard";
import Footer from "./Footer";

const UserProfile = () => {
  const [posts, setPosts] = useState([]);
  const [reactions, setReactions] = useState([]);
  const [comments, setComments] = useState([]);
  const [stories, setStories] = useState([]);
  useEffect(() => {
    const id = localStorage.getItem("userId");
    if (id) {
      console.log("Hi");
      fetch(`http://10.65.1.103:8081/v1.0/post/get-post?userId=${id}`)
        .then((response) => response.json())
        .then((data) => setPosts(data))
        .catch((error) => console.error("Error fetching posts:", error));
    }
  }, []);

  useEffect(() => {
    const id = localStorage.getItem("userId");
    if (id) {
      console.log("Inside get story method");
      fetch(`http://10.65.1.103:8081/v1.0/post/get-story?userId=${id}`)
        .then((response) => response.json())
        .then((data) => {
          console.log("Got stories from backend: ", data);
          setStories(data);
        })
        .catch((error) => console.error("Error fetching stories:", error));
    }
  }, []);

  useEffect(() => {
    // Check if posts array is not empty
    if (posts.length > 0) {
      const postIds = posts.map((post) => post.id);
      fetch(
        "http://10.65.1.103:8082/v1.0/post-interaction/getReactionOnPosts",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            postIds: postIds,
          }),
        }
      )
        .then((response) => response.json())
        .then((data) => setReactions(data))
        .catch((error) => console.error("Error fetching reactions:", error));
    }
  }, [posts]);

  useEffect(() => {
    const fetchCommentsForPost = async (postId) => {
      try {
        const commentsResponse = await fetch(
          `http://10.65.1.188:8084/v1.0/comment/getCommentByPostId/${postId}`
        );
        const commentsData = await commentsResponse.json();
        setComments((prevComments) => [
          ...prevComments,
          { postId: postId, comments: commentsData },
        ]);
      } catch (error) {
        console.error("Error fetching comments:", error);
      }
    };

    posts.forEach((post) => {
      fetchCommentsForPost(post.id);
    });
  }, [posts]);
  return (
    <div>
      <ProfilePageHeader />

      <div className="content" style={{ padding: "95px 0" }}>
        <strong
          style={{
            display: "block",
            textAlign: "center",
            fontSize: "20px",
          }}
        >
          My Stories
        </strong>
        <div className="left-sidebar">
          {stories.map((story) => (
            <MyStoryPostCard key={story.id} post={story} />
          ))}
        </div>
      </div>
      <hr></hr>
      <strong
        style={{
          display: "block",
          textAlign: "center",
          fontSize: "20px",
        }}
      >
        My Posts
      </strong>

      <div className="content" style={{ padding: "95px 0" }}>
        <div className="left-sidebar">
          {posts.map((post, index) => (
            <PostCardProfilePage
              key={post.id}
              post={post}
              reactions={reactions[index]}
              comments={comments.find((c) => c.postId === post.id)?.comments}
            />
          ))}
        </div>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default UserProfile;
