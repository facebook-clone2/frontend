import React from "react";
import { Navbar, NavbarBrand } from "reactstrap";

const HeaderSiteName = () => {
  return (
    <Navbar color="dark" dark expand="md" fixed="top">
      <NavbarBrand className="mr-auto">
        <img
          src={
            "https://letsconnect.world/wp-content/uploads/2019/09/LetsConnect-Icon.png"
          }
          alt="Logo"
          height="30"
        />
      </NavbarBrand>
      <NavbarBrand>
        <strong>Let's Connect</strong>
      </NavbarBrand>
    </Navbar>
  );
};

export default HeaderSiteName;
