import React, { useState } from "react";
import AWS from "aws-sdk";
import { toast } from "react-toastify";
import { Container, Form, FormGroup, Label, Input, Button } from "reactstrap";
import Footer from "./Footer";
import ProfilePageHeader from "./ProfilePageHeader";
import { useNavigate } from "react-router-dom";
const s3 = new AWS.S3({
  region: "ap-south-1",
  accessKeyId: "AKIA3LCNRWEZ5CTRMQ4V",
  secretAccessKey: "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",
});

const UpdateProfileImageForm = () => {
  const navigate = useNavigate();
  const [imageUrl, setImageUrl] = useState("");
  const [fileSelected, setFileSelected] = useState(false);
  const userId = localStorage.getItem("userId");

  const handleFileChange = async (e) => {
    const file = e.target.files[0];
    if (!file) {
      console.error("No file selected");
      return;
    }
    setFileSelected(true); // File selected, enable the submit button

    const allowedImageTypes = ["image/jpeg", "image/png", "image/jpg"];
    if (!allowedImageTypes.includes(file.type)) {
      console.error("Selected file type is not allowed for images");
      alert("Please select a valid image file (JPEG, PNG, JPG).");
      return;
    }
    const reader = new FileReader();
    reader.onload = () => {
      const buffer = reader.result;
      const uint = new Uint8Array(buffer);
      let bytes = [];
      uint.forEach((byte) => {
        bytes.push(byte.toString(16));
      });
      const hex = bytes.join("").toUpperCase();
      const jpegMagicNumber = "FFD8FF";
      const jpgMagicNumber = "FFD8";
      const pngMagicNumber = "89504E47";
      if (
        hex.startsWith(jpegMagicNumber) ||
        hex.startsWith(jpgMagicNumber) ||
        hex.startsWith(pngMagicNumber)
      ) {
        // File is likely an image, proceed with upload
        uploadImage(file);
      } else {
        // File is not an image, notify user
        console.error("Selected file is not a valid image");
        alert("Please select a valid image file (JPEG, JPG, PNG).");
      }
    };
    reader.readAsArrayBuffer(file);
  };

  const uploadImage = async (file) => {
    try {
      const key = `profile-images/${userId}-${Date.now()}-${file.name}`;
      const data = await s3
        .upload({
          Bucket: "engatiinstagram",
          Key: key,
          Body: file,
          ACL: "public-read",
          ContentLength: file.size,
          ContentType: file.type,
        })
        .promise();
      setImageUrl(data.Location);
      console.log("File uploaded successfully:", data.Location);
    } catch (error) {
      console.error("Error uploading file:", error);
    }
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();
    if (!imageUrl) {
      alert("Please select an image to update your profile");
      return;
    }
    try {
      const response = await fetch(
        "http://10.65.1.188:8081/v1.0/users/updateProfileImage",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            id: userId,
            imageUrl: imageUrl,
          }),
        }
      );
      if (response.ok) {
        setImageUrl("");
        console.log("Profile image updated successfully");
        toast.success("Profile image updated successfully");
        navigate("/profile");
      } else if (response.status === 404) {
        console.error("User not found");
        toast.error("User not found");
      } else {
        console.error("Failed to update profile image");
        toast.error("Failed to update profile image. Please try again.");
      }
    } catch (error) {
      console.error("Error during profile image update:", error);
      alert(
        "An error occurred during profile image update. Please try again later."
      );
    }
  };

  return (
    <div>
      <ProfilePageHeader></ProfilePageHeader>
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ minHeight: "70vh" }}
      >
        <div className="col-md-4 mt-5 p-4 shadow rounded">
          <Container>
            <h1 className="mb-4">Update Profile Image</h1>
            <Form onSubmit={handleSubmitForm}>
              <FormGroup>
                <Label for="imageInput">Select Image:</Label>
                <Input
                  type="file"
                  id="imageInput"
                  onChange={handleFileChange}
                />
              </FormGroup>
              <Button
                type="submit"
                color="primary"
                disabled={!fileSelected || !imageUrl}
              >
                Update
              </Button>
            </Form>
          </Container>
        </div>
        <Footer></Footer>
      </div>
    </div>
  );
};

export default UpdateProfileImageForm;
