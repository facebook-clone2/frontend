import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "reactstrap";
import StoryPostCard from "./StoryPostCard";
import ProfilePageHeader from "./ProfilePageHeader";
import Footer from "./Footer";

const StoryPostPage = () => {
  const [postInfo, setPostInfo] = useState([]);
  const userId = localStorage.getItem("userId");

  useEffect(() => {
    fetch(`http://10.65.1.112:8080/friends/getStoryFromFriends/${userId}`)
      .then((response) => response.json())
      .then((data) => setPostInfo(data))
      .catch((error) => console.error("Error fetching data:", error));
  }, [userId]);

  return (
    <div>
      <ProfilePageHeader></ProfilePageHeader>
      <div style={{ padding: "95px 0" }}>
        <strong
          style={{
            display: "block",
            textAlign: "center",
            fontSize: "20px",
          }}
        >
          Friend's Stories
        </strong>
        <Container className="mt-5">
          <Row className="justify-content-center">
            {postInfo.map((post) => (
              <Col key={post.id} md="8">
                <StoryPostCard post={post} />
              </Col>
            ))}
          </Row>
        </Container>
        <Footer></Footer>
      </div>
    </div>
  );
};

export default StoryPostPage;
