import React, { useEffect, useState } from "react";
import { useParams, useLocation, navigate } from "react-router-dom";
import PeopleCard from "./PeopleCard";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import ProfilePageHeader from "./ProfilePageHeader";
import Footer from "./Footer";
const SearchResult = () => {
  const [searchResults, setSearchResults] = useState([]);
  const { searchText } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  useEffect(() => {
    const trimmedSearchText = searchText.trim(); // Trim leading and trailing spaces
    if (!trimmedSearchText) {
      // If searchText becomes empty after trimming or contains only whitespace
      toast.error("Please enter a valid search query");
      // Redirect to the current location if search query is invalid
      navigate(location.pathname);
      return;
    }

    const fetchData = async () => {
      try {
        const response = await fetch(
          `http://10.65.1.188:8081/v1.0/users/searchByName/${trimmedSearchText}`
        );
        if (response.ok) {
          const data = await response.json();
          setSearchResults(data);
        } else {
          toast.error("No results");
        }
      } catch (error) {
        console.error("Error during search result fetch:", error);
      }
    };

    fetchData();
  }, [searchText, location]);

  return (
    <div>
      <ProfilePageHeader />
      <div style={{ padding: "75px 0" }}>
        <style>{`
      .search-results-container {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around; /* Adjust as needed */
      }

      /* Center the text */
      .search-results-header {
        text-align: center;
        margin-top: 20px; /* Adjust the margin as needed */
      }

    `}</style>
        <p className="search-results-header">
          Search Results for: {searchText}
        </p>
        <div className="search-results-container">
          {searchResults.length === 0 ? (
            <p>No users found</p>
          ) : (
            <div
              className="search-results-container"
              style={{
                display: "flex",
                flexWrap: "wrap",
                justifyContent: "flex-start", // Align cards from the left
                marginLeft: "20px",
                gap: "20px", // Adjust the gap as needed
              }}
            >
              {searchResults.map((result) => (
                <PeopleCard
                  key={result.id}
                  id={result.id}
                  imageUrl={result.imageUrl}
                  name={result.name}
                />
              ))}
            </div>
          )}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default SearchResult;
